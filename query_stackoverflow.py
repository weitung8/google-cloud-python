import pandas
import matplotlib.pyplot as plt
from google.cloud import bigquery

def query_stackoverflow():
    client = bigquery.Client()

    # query google bigquery questions
    df_questions = client.query(
        """
        SELECT
            CONCAT(
                'https://stackoverflow.com/questions/',
                CAST(id as STRING)) as url,
            view_count
        FROM `bigquery-public-data.stackoverflow.posts_questions`
        WHERE tags like '%google-bigquery%'
        ORDER BY view_count DESC
        LIMIT 10
        """
    )

    print(df_questions)
    #print(type(df_questions))
    # for row in df_questions:
    #     print(row)


    # query most reputed stackoverflow uses
    df_users = client.query(
        """
        SELECT
           display_name,
           reputation,
           location
        FROM `bigquery-public-data.stackoverflow.users`
        ORDER BY reputation desc
        LIMIT 200
        """
    ).to_dataframe()
   
    df_users = df_users.set_index('display_name')
    df_users = df_users.head(15)
    print(df_users)

    chart = df_users.plot(title='users reputation',
                        xlabel='user name',
                        ylabel='reputation',
                        legend=True,  # 是否顯示圖例
                        figsize=(10, 5),
                        marker='o') #線節點圖示
    
    plt.show()
    
    df_users_reputation_by_location = df_users[["location", "reputation"]].groupby("location").sum()
    print(df_users_reputation_by_location)
    print(df_users_reputation_by_location.sort_values("reputation", ascending=False))
    print(df_users[["location", "reputation"]].groupby("location")["location"].count())
    print(df_users["location"].value_counts())
    
    # for row in results:
    #     print(f"{row.display_name} in {row.location} with {row.reputation} reputations")
    
if __name__ == "__main__":
    query_stackoverflow()
