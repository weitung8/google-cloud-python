import os
import wave
import contextlib

directory = "/root/sound"
files = ["cantina-band-3.wav", "cantina-band-60.wav"]

for f in files:
    path = os.path.join(directory, f)

    with contextlib.closing(wave.open(path, 'r')) as wav_file:
        frames = wav_file.getnframes()
        rate = wav_file.getframerate()
        durations = frames / float(rate)
        print(f"file: {path}, duration: {durations}")

